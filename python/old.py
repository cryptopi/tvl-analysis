from sympy import *
from sympy.stats import Multinomial, Normal, density

init_printing()

s, i, count = symbols('s i, count')

# Hyperparameters
mu = 0.6
sigma = 0.2
p_lucky = 0.2
p_unlucky = 0.2
iters = 80


skill_dist = Normal('skill_dist', mu, sigma)
# noinspection PyCallingNonCallable
wealth_dist = Multinomial('wealth_dist', iters, p_lucky, p_unlucky, 1 - p_lucky - p_unlucky)
p_skill = density(skill_dist)(s)
wealth_density = density(wealth_dist)
p_wealth_given_skill = Sum(wealth_density(i + count, i, iters - 2 * i - count), (i, 0, (iters - count) // 2))
joint_probability = p_skill * p_wealth_given_skill

marginal_wealth_dist = integrate(joint_probability, (s, -oo, oo))

if __name__ == '__main__':
    marginal_func = lambdify(count, marginal_wealth_dist, "math")
    print(marginal_func(10))
    # joint_func = lambdify([s, count], joint_probability, "math")
    # print(joint_func(1, 0))
