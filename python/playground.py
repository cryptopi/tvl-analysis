import numpy as np
from utils import nested_for_loops, pop, get_bucket_sample_points
import seaborn as sns
import matplotlib.pyplot as plt
from grid_compute import grid_compute, get_results_from_array
from scipy.interpolate import interp1d
import time


def busy_wait(dt):
    current_time = time.time()
    while time.time() < current_time + dt:
        pass


def compute_func(*args):
    print(f'Computing for {args}')
    # busy_wait(1)
    time.sleep(1)
    return np.sum(np.power(3, np.arange(len(args))) * np.array(list(reversed(args))))


if __name__ == '__main__':

    # partitions = (2, 2, 2, 3, 4, 5)
    # full = np.empty(partitions, dtype=int)
    #
    # def assign_default(*indices):
    #     full[indices] = np.sum(np.power(3, np.arange(len(indices))) * np.array(list(reversed(indices))))
    #
    # nested_for_loops(partitions, assign_default)
    #
    # if len(partitions) % 2 == 0:
    #     is_row_index = lambda i: i % 2 == 1
    # else:
    #     is_row_index = lambda i: i != 0 and (i - 1) % 2 == 1
    # column_counts = [partitions[i] for i in filter(lambda i: not is_row_index(i), range(len(partitions)))]
    # row_counts = [partitions[i] for i in filter(is_row_index, range(len(partitions)))]
    #
    # grid = np.empty((np.product(row_counts), np.product(column_counts)), dtype=int)
    #
    # def assign_box_to_grid(*indices):
    #     start_row = 0
    #     start_col = 0
    #     rows_so_far = 0
    #     cols_so_far = 0
    #     for i in range(len(indices)):
    #         index = indices[i]
    #         if is_row_index(i):
    #             start_row += index * int(np.product(row_counts[rows_so_far + 1:]))
    #             rows_so_far += 1
    #         else:
    #             start_col += index * int(np.product(column_counts[cols_so_far + 1:]))
    #             cols_so_far += 1
    #     grid[start_row:start_row + row_counts[-1], start_col:start_col + column_counts[-1]] = full[indices].T
    #
    # nested_for_loops(partitions[:-2], assign_box_to_grid)


    # print(grid)
    #
    # fig = plt.figure()
    # ax = fig.gca()
    # sns.heatmap(grid, ax=ax)
    # ax.invert_yaxis()
    #
    # plt.show()

    # x = np.linspace(0, 1, 5)
    # values = np.arange(20).reshape((5, -1))
    # new_bucket_count = 2
    # bucket_x = get_bucket_sample_points(x[0], x[-1], new_bucket_count)
    # print(x)
    # print(values)
    # print(bucket_x)
    #
    # bucket_func = interp1d(x, values, axis=0)
    # print(bucket_func(np.array(bucket_x)))

    names = ('a', 'b')
    bounds = [(0, 1), (0, 1)]
    threads = 1

    watch_filename = '~/Desktop/explore/watch'
    save_filename = '~/Desktop/explore/save'
    load_filename = save_filename
    # load_filename = None

    pruned, tree = grid_compute(names, bounds, compute_func, threads, watch_filename, save_filename, load_filename)
    print(tree)
    print(pruned)

    results_array = pruned.to_array()
    print(results_array)
    print('Results:')
    print(get_results_from_array(results_array))


