import numpy as np
import math
from scipy.interpolate import interp1d

def nested_for_loops(stops, func, starts=None):
    def helper (starts, stops, func, args, index):
        if len(stops) == 0:
            return [func(*args)]
        stop, *rest_stop = stops
        if starts is not None:
            start, *rest_start = starts
        else:
            start = 0
            rest_start = None
        results = []
        for i in range(start, stop):
            args[index] = i
            results = results + helper(rest_start, rest_stop, func, args, index + 1)
        return results

    return helper(starts, stops, func, [0] * len(stops), 0)


def pop(arr, default_factory):
    if arr is None:
        return default_factory(), arr
    else:
        popped, *rest = arr
        return popped, rest


def simplyify_frac(num, denom):
    gcd = math.gcd(num, denom)
    return num // gcd, denom // gcd


def get_bucket_values(sample_points, values, bucket_sample_points):
    if len(values) == 1:
        return np.repeat(values[0], len(bucket_sample_points))
    bucket_func = interp1d(sample_points, values, axis=0, bounds_error=False,
                           fill_value=(values[0], values[-1]))
    return bucket_func(bucket_sample_points)


# def resample(arr, sample_count):
#     old_sample_count = len(arr)
#     if old_sample_count < sample_count:
#         return upsample(arr, sample_count)
#     else:
#         return downsample(arr, sample_count)


# def downsample(arr, sample_count):
#     raise NotImplementedError('Cannot downsample yet')
#
#
# def upsample(arr, sample_count):
#     old_sample_count = arr.shape[0]
#     if old_sample_count == sample_count:
#         return arr
#     if old_sample_count > sample_count:
#         raise ValueError(f'Cannot upsample from {old_sample_count} to {sample_count}')
#     upsampled_elements = []
#     last_edge = 0
#     last_old_sample_index = 0
#     old_sample_width = 1 / old_sample_count
#     new_sample_width = 1 / sample_count
#     _, integer_steps_of_new = simplyify_frac(old_sample_count, sample_count)
#     for i in range(sample_count):
#         current_edge = (i + 1) * new_sample_width
#         current_old_sample_index = int(current_edge / old_sample_width)
#         # Include trailing edge on previous bucket
#         if (i + 1) % integer_steps_of_new == 0:
#             current_old_sample_index -= 1
#         if last_old_sample_index == current_old_sample_index:
#             upsampled_elements.append(arr[current_old_sample_index])
#         else:
#             straddle_edge = current_old_sample_index * old_sample_width
#             fraction_last = (straddle_edge - last_edge) / new_sample_width
#             fraction_current = (current_edge - straddle_edge) / new_sample_width
#             upsampled_elements.append(fraction_last * arr[last_old_sample_index] +
#                                       fraction_current * arr[current_old_sample_index])
#
#         last_edge = current_edge
#         last_old_sample_index = current_old_sample_index
#
#     return np.stack(upsampled_elements)


def get_bucket_sample_points(lower, upper, count):
    width = (upper - lower) / count
    sampled = []
    for i in range(count):
        min_bucket = i * width
        max_bucket = (i + 1) * width
        sampled.append((min_bucket + max_bucket) / 2)
    return sampled


def add_bounds(one, two):
    if one[0] < two[0]:
        first = one
        second = two
    else:
        first = two
        second = one

    if first[1] != second[0]:
        raise ValueError(f'Bounds must be contiguous, got {first} and {second}')

    first_min, first_max = first
    second_min, second_max = second
    return first_min, second_max

