from matplotlib.ticker import FuncFormatter


class ValueAxisFormatter(FuncFormatter):

    def __init__(self, min_value, max_value, size, format):
        super(ValueAxisFormatter, self).__init__(
            ValueAxisFormatter.get_function(min_value, max_value, size, format)
        )

    @staticmethod
    def get_function(min_value, max_value, size, fmt):
        def func(x, pos):
            value = min_value + (max_value - min_value) * x / size
            return fmt % value
        return func
