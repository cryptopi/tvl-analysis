from simulation_params import SimulationParams
import bisect
from pathlib import Path
import pickle
import multiprocessing as mp
import numpy as np
import math
import itertools
from utils import nested_for_loops, pop, add_bounds, get_bucket_sample_points, get_bucket_values
from copy import deepcopy
from functools import reduce
from operator import itemgetter
from multiprocessing import Pool, Queue, Process, Event as MultiprocessingEvent
from multiprocessing.managers import BaseManager
import queue
from scipy.interpolate import interp1d
from heapq import heappush, heappop, heapify
import time
from itertools import chain
from collections import defaultdict


def get_results_from_array(arr):
    if len(arr) == 1 and not isinstance((arr[0]), tuple):
        return arr
    results = []
    for tup in arr:
        results.extend(get_results_from_array(tup[1]))
    return results


def to_grid(packed):
    partition_counts = packed.shape

    if len(partition_counts) % 2 == 0:
        is_row_index = lambda i: i % 2 == 1
    else:
        is_row_index = lambda i: i != 0 and (i - 1) % 2 == 1
    column_counts = [partition_counts[i] for i in filter(lambda i: not is_row_index(i), range(len(partition_counts)))]
    row_counts = [partition_counts[i] for i in filter(is_row_index, range(len(partition_counts)))]

    grid = np.empty((np.product(row_counts), np.product(column_counts)), dtype=int)

    def assign_box_to_grid(*indices):
        start_row = 0
        start_col = 0
        rows_so_far = 0
        cols_so_far = 0
        for i in range(len(indices)):
            index = indices[i]
            if is_row_index(i):
                start_row += index * int(np.product(row_counts[rows_so_far + 1:]))
                rows_so_far += 1
            else:
                start_col += index * int(np.product(column_counts[cols_so_far + 1:]))
                cols_so_far += 1
        grid[start_row:start_row + row_counts[-1], start_col:start_col + column_counts[-1]] = packed[indices].T

    nested_for_loops(partition_counts[:-2], assign_box_to_grid)

    return grid


class ValueInterval:

    def __init__(self, lower, upper, reverse):
        self.lower = lower
        self.upper = upper
        self.length = upper - lower
        self.reverse = reverse

    def get_middle(self):
        return (self.lower + self.upper) / 2

    def get_sample_point(self):
        return self.get_middle()

    def __lt__(self, other):
        if self.reverse:
            return self.length > other.length
        else:
            return self.length < other.length


class ValueIntervalStore:

    def __init__(self):
        self.smallest = []
        self.largest = []

    @staticmethod
    def from_values(lower, upper, values):
        store = ValueIntervalStore()
        values = [lower] + values + [upper]
        for i in range(1, len(values)):
            store.add(values[i - 1], values[i])
        return store

    def pop_max(self):
        return heappop(self.largest)

    def peek_min(self):
        return self.smallest[0]

    def add(self, lower, upper):
        forward = ValueInterval(lower, upper, reverse=False)
        backward = ValueInterval(lower, upper, reverse=True)
        heappush(self.smallest, forward)
        heappush(self.largest, backward)


class Node:
    ROUND_PRECISION = 4

    def __init__(self, variable_name, lower_bound, upper_bound, level):
        self.variable_name = variable_name
        self.lower_bound = lower_bound
        self.upper_bound = upper_bound
        self.level = level
        self.children = {}
        self.intervals = ValueIntervalStore()

    @staticmethod
    def get_key(value):
        if type(value) == 'str':
            print('hi')
        return round(value, Node.ROUND_PRECISION)

    @staticmethod
    def get_value(key):
        return key

    def set_initial_child(self, value, child):
        key = Node.get_key(value)
        self.children[key] = child
        self.intervals.add(self.lower_bound, value)
        self.intervals.add(value, self.upper_bound)

    def get_child(self, value):
        return self.children[Node.get_key(value)]

    def get_sorted_children(self):
        keys = list(self.children.keys())
        sorted_keys = sorted(keys, key=lambda key: Node.get_value(key))
        sorted_values = [Node.get_value(key) for key in sorted_keys]
        return sorted_values, [self.children[key] for key in sorted_keys]

    def get_sample_child(self):
        return list(self.children.values())[0]

    def get_middle(self):
        return (self.lower_bound + self.upper_bound) / 2

    # TODO fix this to something better?
    # Basically sampled is the amount of buckets we want to have data on
    def get_sample_points(self, count):
        return get_bucket_sample_points(self.lower_bound, self.upper_bound, count)

    def store_result(self, values, result):
        current, *rest = values
        child = self.get_child(current)
        if child is not None:
            return child.store_result(rest, result)
        return False

    def mark_result_pending(self, values):
        current, *rest = values
        child = self.get_child(current)
        if child is not None:
            return child.mark_result_pending(rest)
        return False

    def get_pending_count(self):
        return sum([child.get_pending_count() for child in self.children.values()])

    def clear_all_pending(self):
        for child in self.children.values():
            child.clear_all_pending()

    def clone_empty(self, resample=True):
        cloned = Node(self.variable_name, self.lower_bound, self.upper_bound, self.level)
        child_list = list(self.children.values())
        if resample:
            values_list = self.get_sample_points(len(child_list))
            intervals = self.intervals
        else:
            values_list = [Node.get_value(key) for key in self.children]
            intervals = ValueIntervalStore.from_values(self.lower_bound, self.upper_bound, values_list)
        for i in range(len(child_list)):
            key = Node.get_key(values_list[i])
            cloned.children[key] = child_list[i].clone_empty(resample=True)
        cloned.intervals = intervals
        return cloned

    def split(self, resample=True, add=0, multiply=0):
        count_to_add = len(self.children) * multiply + add
        if count_to_add == 0:
            raise ValueError('Must add at least one child during split')

        sample_child = self.get_sample_child()
        for i in range(count_to_add):
            largest = self.intervals.pop_max()
            new_value = largest.get_sample_point()
            new_key = Node.get_key(new_value)
            self.intervals.add(largest.lower, new_value)
            self.intervals.add(new_value, largest.upper)
            self.children[new_key] = sample_child.clone_empty(resample=resample)

    def split_level(self, level_to_split, resample=True, add=0, multiply=0):
        if self.level == level_to_split:
            self.split(resample, add=add, multiply=multiply)
        else:
            # Could bubble up is_computed=False here
            for child in self.children.values():
                child.split_level(level_to_split, resample=resample, add=add, multiply=multiply)

    # def partition(self, partition_counts):
    #     count, *rest = partition_counts
    #     partitions = []
    #     partition_width = (self.upper_bound - self.lower_bound) / count
    #
    #     for i in range(count):
    #         lower = i * partition_width + self.lower_bound
    #         upper = lower + partition_width
    #         child_keys = [key for key in self.children if lower <= Node.get_value(key) < upper]
    #         child_list = [self.children[key] for key in child_keys]
    #         partitioned_children = [child.partition(rest) for child in child_list]
    #         for children in itertools.product(*partitioned_children):
    #             children = {child_keys[i]: children[i] for i in range(len(child_keys))}
    #             partition = Node(self.variable_name, lower, upper, self.level, len(children))
    #             partition.children = children
    #             partitions.append(partition)
    #     return partitions

    def get_values_to_compute(self):
        # TODO performance: mark nodes as already computed so don't have to traverse
        to_compute = []
        for key in self.children:
            value = Node.get_value(key)
            child = self.children[key]
            if not self.has_node_children():
                if child.is_computed() or child.is_pending():
                    continue
                else:
                    to_compute.append((value,))
            else:
                for child_compute in child.get_values_to_compute():
                    to_compute.append((value,) + child_compute)
        return to_compute

    def has_node_children(self):
        return isinstance(list(self.children.values())[0], Node)

    # INVARIANT: tree is fully balanced at all times
    def get_child_counts(self):
        if self.has_node_children():
            return [len(self.children)] + [self.get_sample_child().get_child_counts()]
        else:
            return [len(self.children)]

    # TODO make better heuristic
    def compute_bucket_count(self):
        smallest_width = self.intervals.peek_min().length
        print(f'Node: {self.variable_name} has min width {smallest_width}')
        return int(math.ceil((self.upper_bound - self.lower_bound) / smallest_width))

    def get_bucket_counts(self):
        if self.has_node_children():
            # All children have same bucket count
            return [self.compute_bucket_count()] + list(self.children.values())[0].get_bucket_counts()
        else:
            return [self.compute_bucket_count()]

    def get_max_bucket_counts(self):
        if self.has_node_children():
            max_child_bucket_counts = [child.get_max_bucket_counts() for child in self.children.values()]
            return [self.compute_bucket_count()] + list(np.max(max_child_bucket_counts, axis=0))
        else:
            return [self.compute_bucket_count()]

    def pack(self, bucket_counts=None):
        bucket_count, rest_bucket_counts = pop(bucket_counts, lambda: self.compute_bucket_count())

        sorted_values, sorted_children = self.get_sorted_children()
        if self.has_node_children():
            packed_children = [child.pack(rest_bucket_counts) for child in sorted_children]
        else:
            packed_children = np.empty(len(self.children))
            for i in range(len(self.children)):
                packed_children[i] = sorted_children[i].result

        return get_bucket_values(
            sorted_values,
            packed_children,
            get_bucket_sample_points(self.lower_bound, self.upper_bound, bucket_count)
        )

    def to_array(self):
        arr = []
        values, children = self.get_sorted_children()
        for i in range(len(values)):
            child = children[i]
            if child.is_computed():
                arr.append((values[i], child.to_array()))
        return arr

    def is_computed(self):
        return all([child.is_computed() for child in self.children.values()])

    def merge_with(self, other):
        raise NotImplementedError('Node merge_with not implemented')

    def prune(self):
        keys = list(self.children.keys())
        for key in keys:
            should_delete = self.children[key].prune()
            if should_delete:
                del self.children[key]
        all_values = list(sorted([Node.get_value(key) for key in self.children]))
        self.intervals = ValueIntervalStore.from_values(self.lower_bound, self.upper_bound, all_values)
        return len(self.children) == 0

    def __str__(self):
        text = ''
        indent = ' ' * self.level * 4
        text += f'{indent}Node: {self.variable_name}\n'
        text += f'{indent}Child count: {len(self.children)}\n'
        for key in self.children:
            text += f'{indent}Value: {Node.get_value(key)}\n'
            text += str(self.children[key])
        text += '\n'
        return text



class Result:

    def __init__(self, result, level, pending=False):
        self.result = result
        self.level = level
        self.pending = pending

    def is_computed(self):
        return self.result is not None

    def is_pending(self):
        return self.pending

    def store_result(self, values, result):
        self.result = result
        self.pending = False

    def mark_result_pending(self, values):
        self.pending = True

    def get_pending_count(self):
        return 1 if self.pending else 0

    def clear_all_pending(self):
        self.pending = False

    def clone_empty(self, resample):
        return Result(None, self.level)

    def partition(self, partition_counts):
        return self

    def prune(self):
        return not self.is_computed()

    def to_array(self):
        return [self.result]

    def __str__(self):
        indent = ' ' * self.level * 4
        text = 'Pending' if self.is_pending() else f'{self.result}'
        return f'{indent}Result: {text}\n'



class ComputeTree:

    def __init__(self, variable_names, bounds, root, child_counts, iteration):
        self.variable_names = variable_names
        self.bounds = bounds
        self.root = root
        self.child_counts = child_counts
        self.iteration = iteration
        self.before_last_split = None
        self.current_split = 0
        # self.min_pending_split = 0
        self.pending_computations = defaultdict(set)

    @staticmethod
    def init(variable_names, bounds):
        root = ComputeTree.make_node(variable_names, bounds)
        bucket_counts = [1] * len(variable_names)
        tree = ComputeTree(variable_names, bounds, root, bucket_counts, 0)

        pending = root.get_values_to_compute()
        for value_list in pending:
            tree.pending_computations[tree.current_split].add(value_list)
        return tree

    @staticmethod
    def make_node(variable_names, bounds):
        root = None
        parent = None
        for variable_name, bound in zip(variable_names, bounds):
            level = 0 if parent is None else parent.level + 1
            child = Node(variable_name, bound[0], bound[1], level)
            if parent is None:
                root = child
                parent = child
            else:
                parent_value = parent.get_sample_points(1)[0]
                parent.set_initial_child(parent_value, child)
                parent = child
        parent_value = parent.get_sample_points(1)[0]
        parent.set_initial_child(parent_value, Result(None, parent.level + 1))
        return root

    @staticmethod
    def read(filename):
        path = Path(filename).expanduser()
        with open(path, 'rb') as f:
            return pickle.load(f)

    def write(self, filename):
        to_write = deepcopy(self)
        to_write.clear_all_pending()
        to_write.current_split = 0 # to_write.min_pending_split
        path = Path(filename).expanduser()
        with open(path, 'wb') as f:
            pickle.dump(to_write, f)

    def annotate_values(self, values):
        annotated = []
        for value_list in values:
            annotated.append({self.variable_names[i]: value_list[i] for i in range(len(value_list))})
        return annotated

    def split(self):
        print('Splitting')
        self.current_split += 1
        level_to_split = np.argmin(self.child_counts)
        self.root.split_level(level_to_split, add=1)
        self.child_counts[level_to_split] += 1

    def is_computed(self):
        return self.root.is_computed()

    def get_values_to_compute(self):
        while self.current_split < len(self.pending_computations):
            pending = self.pending_computations[self.current_split]
            current_split = self.current_split
            self.current_split += 1
            if len(pending) > 0:
                print(f'Got {len(pending)} computations for split {current_split}')
                return current_split, self.annotate_values(pending)

        # If not pending, must need split
        self.split()
        values = self.root.get_values_to_compute()
        for value_tuple in values:
            self.pending_computations[self.current_split].add(value_tuple)
        return self.current_split, self.annotate_values(values)

        # values = self.root.get_values_to_compute()
        # if len(values) == 0:
        #     self.split()
        #     values = self.root.get_values_to_compute()
        # for value_tuple in values:
        #     self.pending_computations[self.current_split].add(value_tuple)
        # return self.current_split, self.annotate_values(values)

    def mark_result_pending(self, values):
        self.root.mark_result_pending(values)

    def get_pending_count(self):
        return self.root.get_pending_count()

    def store_result(self, for_split, values, result):
        values = tuple(values)
        if values not in self.pending_computations[for_split]:
            raise ValueError('Could not find calculation in pending calculations')
        self.pending_computations[for_split].remove(values)
        # if len(self.pending_computations[for_split]) == 0:
        #     if for_split == self.min_pending_split:
        #         del self.pending_computations[for_split]
        #         self.min_pending_split += 1
        self.root.store_result(values, result)

    def clear_all_pending(self):
        self.root.clear_all_pending()

    def prune(self):
        self.root.prune()
        return self

    def partition(self, partition_count):
        raise NotImplementedError('Partition not implemented yet, use pool for task processing')
        # roots = self.root.partition(partition_counts)
        # partitions = []
        # new_split_counts = np.add(partition_counts, self.split_counts)
        # for root in roots:
        #     partition = ComputeTree(self.variable_names, root, new_split_counts, self.iteration)
        #     partitions.append(partition)
        # return partitions

        # Just partition top for this purpose, in general need to partition n-dimensional space
        # see commented out partition method in node
        # partition_width = (self.root.upper_bound - self.root.lower_bound) / partition_count
        # root_partitions = []
        # for i in range(partition_count):
        #     lower = i * partition_width + self.root.lower_bound
        #     upper = lower + partition_width
        #     root = Node(self.root.variable_name, lower, upper, self.root.level)
        #     child_keys = [key for key in self.root.children if lower <= Node.get_value(key) < upper]
        #     children = {key: self.root.children[key] for key in child_keys}
        #     if len(children) == 0:
        #         child = ComputeTree.make_node(self.variable_names[1:], self.bounds[1:])
        #         value = root.get_sample_points(1)[0]
        #         children = {Node.get_key(value): child}
        #     root.children = children
        #     root_partitions.append(root)
        # tree_partitions = []
        # for root in root_partitions:
        #     new_bounds = [root.lower_bound, root.upper_bound]
        #     new_bounds.extend(self.bounds[1:])
        #     partition = ComputeTree(self.variable_names, new_bounds, root, root.get_child_counts(), 0)
        #     tree_partitions.append(partition)
        #
        # return Forest(tree_partitions, self.iteration)

    # Warning! mutates self & other
    # Other must come after self in root bounds
    def merge_with(self, other):
        raise NotImplementedError('Merge not implemented')
        # Merging along root only
        # self.bounds[0] = (self.bounds[0][0], other.bounds[0][1])
        # self.root.merge_with(other.root)
        # self.bucket_counts[0] = self.root.bucket_count
        # self.iteration += other.iteration
        # return self

    def pack(self, kind='upsample'):
        if kind != 'upsample':
            raise NotImplementedError('Only to_grid kind "upsample" supported')

        # Need min_bucket_count if downsample is ever supported

        counts = self.root.get_max_bucket_counts()
        print(f'Max bucket counts: {counts}')
        values = self.root.pack(counts)
        return values

    def to_array(self):
        return self.root.to_array()

    def __str__(self):
        text = ''
        text += 'Compute Tree with root:'
        text += str(self.root) + '\n'
        text += f'Pending calculations:\n'
        for split in self.pending_computations:
            text += f'   {split}\n'
            for computation in self.pending_computations[split]:
                text += f'         {computation}\n'
        return text


class Forest:

    def __init__(self, trees, previous_iteration_count):
        self.trees = trees
        self.previous_iteration_count = previous_iteration_count

    def merge(self):
        raise NotImplementedError('Merge not implemented')
        # ordered = sorted(self.trees, key=lambda tree: tree.root.lower_bound)
        # merged = reduce(lambda a, b: a.merge(b), ordered)
        # merged.iteration += self.previous_iteration_count
        # return merged


class MyManager(BaseManager):
    pass


def get_manager():
    manager = MyManager()
    manager.start()
    return manager


class SharedState:

    def __init__(self, bucket_counts):
        self.bucket_counts = bucket_counts

    def get_root_count(self, process_index):
        return self.bucket_counts[process_index][0]

    def set_root_count(self, process_index, count):
        self.bucket_counts[process_index][0] = count


MyManager.register('SharedState', SharedState)


def consume_work(shutdown_event, work_queue, results_queue, compute_func):
    print('Process running')

    while not shutdown_event.is_set():
        split, values = work_queue.get()
        result = compute_func(*values)
        results_queue.put((split, values, result))


def grid_compute(names, bounds, compute_func, threads, watch_filename, save_filename, load_filename=None):
    tree = None
    if load_filename is not None:
        load_path = Path(load_filename).expanduser()
        if load_path.is_file():
            tree = ComputeTree.read(load_path)
            print(f'Loaded tree')
            print(f'Previous iterations: {tree.iteration}')

    if tree is None:
        tree = ComputeTree.init(names, bounds)

    watch_path = Path(watch_filename).expanduser()
    save_path = Path(save_filename).expanduser()
    CHECK_INTERVAL = 1
    MAX_WORK_SIZE = 100
    SAVE_INTERVAL = 600
    get_value_list = itemgetter(*names)
    work_queue = Queue(MAX_WORK_SIZE)
    results_queue = Queue()

    # Set up processes
    shutdown_event = MultiprocessingEvent()
    processes = []
    for i in range(threads):
        p = Process(target=consume_work, args=(shutdown_event, work_queue, results_queue, compute_func))
        p.start()
        processes.append(p)

    loop_iteration = 0
    last_check_time = time.time()
    last_save_time = time.time()
    results_computed = 0
    while True:
        current_split, values_to_compute = tree.get_values_to_compute()
        i = 0
        while i < len(values_to_compute):
            values = values_to_compute[i]
            # print(f'Iteration: {tree.iteration}')
            # print(f'Values: {values}')
            loop_iteration += 1

            value_list = get_value_list(values)
            try:
                work_queue.put((current_split, value_list), timeout=1)
                tree.mark_result_pending(value_list)
                i += 1
            except queue.Full:
                while True:
                    try:
                        # print('Trying to get result')
                        head = results_queue.get(block=False)
                        results_computed += 1
                        split, values, result = head
                        tree.store_result(split, values, result)
                        tree.iteration += 1
                        print(f'Computed {results_computed} results')
                    except queue.Empty:
                        break

            current_time = time.time()
            if current_time >= last_check_time + CHECK_INTERVAL:
                last_check_time = current_time
                if watch_path.is_file():
                    with open(watch_path, 'r') as f:
                        lines = f.readlines()
                        if len(lines) > 0:
                            text = lines[0].strip().lower()
                            if text == 'stop':
                                shutdown_event.set()
                                for process in processes:
                                    process.join()
                                    process.kill()
                                    process.close()
                                tree.clear_all_pending()
                                tree.write(save_path)
                                return deepcopy(tree).prune(), tree
                            elif text == 'progress':
                                print(f'{tree.get_pending_count()} results pending')
                            elif text == 'save':
                                clone = deepcopy(tree)
                                clone.clear_all_pending()
                                clone.write(save_path)

            if current_time >= last_save_time + SAVE_INTERVAL:
                print('Autosaving')
                last_save_time = current_time
                tree.write(save_path)


    # forest = None
    # if load_filename is not None:
    #     load_path = Path(load_filename)
    #     if load_path.is_file():
    #         forest = pickle.load(load_filename)
    #         print(f'Loaded forest')
    #         print(f'Previous iterations: '
    #               f'{forest.previous_iteration_count + sum([tree.iteration for tree in forest.trees])}')
    #         print(f'Tree count: {len(forest.trees)}')
    # if forest is None:
    #     tree = ComputeTree.init(names, bounds)
    #     forest = Forest([tree], 0)
    #
    # if threads != len(forest.trees):
    #     merged = forest.merge()
    #     forest = merged.partition(threads)
    #
    # with Pool(processes=threads) as pool:
    #     futures = []
    #     for i in range(threads):
    #         futures.append(pool.apply_async(run_computation, (names, forest.trees[i], compute_func, watch_filename)))
    #
    #     trees = []
    #     for i in range(threads):
    #         future = futures[i]
    #         tree = future.get()
    #         tree = tree if tree is not None else forest.trees[i]
    #         trees.append(tree)
    #
    #     completed_forest = Forest(trees, 0)
    #     packed = completed_forest.merge().pack(type='upsample')
    #     return to_grid(packed), completed_forest
