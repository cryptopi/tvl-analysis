from scipy.stats import truncnorm, norm, multinomial, pareto, linregress, \
    invgauss
import numpy as np
from toolz.functoolz import curry
from scipy.integrate import quad as sciquad
from scipy.interpolate import make_interp_spline, BSpline, interpolate
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.ticker import LinearLocator, MultipleLocator
# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D
from tqdm import tqdm
import seaborn as sns
import pandas as pd
from ValueAxisFormatter import ValueAxisFormatter
from simulation_params import SimulationParams
from scipy.special import factorial
import bisect
from grid_compute import grid_compute
from scipy.integrate import odeint

sns.set()

EVENT_LABEL = 'Net Event Count'


def integrate(func, lower, upper, include_error=False):
    value = sciquad(func, lower, upper)
    if include_error:
        return value
    return value[0]


def spline_interpolate(x, y, grid, k=3):
    spline = make_interp_spline(x, y, k=k)
    return spline(grid)


def get_talent_dist(mu, sigma, lowest=-np.inf, highest=np.inf):
    if not np.isinf(lowest) and not np.isinf(highest):
        return norm(mu, sigma)
    else:
        a = (lowest - mu) / sigma
        b = (highest - mu) / sigma
        return truncnorm(a, b, loc=mu, scale=sigma)


def make_multivariate_from_talent(talent_dist):
    def multivariate(talent, lucky_count):
        return talent_dist.pdf(talent)
    return curry(multivariate)


def get_conditional_wealth_dist_family(pu, pl, iters, utilize_probability=lambda s: s):
    def conditional_dist(talent):
        pl_real = pl * max(min(utilize_probability(talent), 1), 0)
        wealth_dist = multinomial(iters, [pu, pl_real, 1 - pu - pl_real])

        def density(lucky_count):
            net_count = abs(lucky_count)
            if lucky_count >= 0:
                wealth_cases = [[i, i + net_count, iters - 2 * i - net_count] for i in
                                range((iters - net_count) // 2 + 1)]
            else:
                wealth_cases = [[i + net_count, i, iters - 2 * i - net_count] for i in
                                range((iters - net_count) // 2 + 1)]
            all_wealth_densities = wealth_dist.pmf(wealth_cases)
            total = np.sum(all_wealth_densities)
            if total < 0:
                print(f'Talent: {talent}, lucky count: {lucky_count} = {total}')
            return total
        return density
    return conditional_dist


def get_joint_distribution(talent_dist, conditional_wealth_dist_family):

    def joint(talent, lucky_count):
        conditional_wealth_dist = conditional_wealth_dist_family(talent)
        log_talent_density = talent_dist.logpdf(talent)
        wealth_density = conditional_wealth_dist(lucky_count)
        if wealth_density == 0:
            return 0
        else:
            log_wealth_density = np.log(wealth_density)
            joint_density = log_talent_density + log_wealth_density
            return np.exp(joint_density)

    return curry(joint)


def sum_joint_distribution(joint_distribution, iters):
    total = 0
    error = 0
    for count in range(-iters, iters + 1):
        marginal = get_marginal_wealth_distribution(joint_distribution, include_error=True)
        value = marginal(count)
        total += value[0]
        error += value[1]
    return total, error


def get_marginal_wealth_distribution(joint_distribution, include_error=False):
    def marginal_mass(lucky_count):
        partial = joint_distribution(lucky_count=lucky_count)
        return integrate(partial, -np.inf, np.inf, include_error=include_error)
    return marginal_mass


def show_plot():
    plt.show()


def save_plot(filename):
    plt.savefig(filename)


def evaluate_marginal_wealth(marginal_wealth_distribution, start, stop, show_progress=True):
    x_grid = np.arange(start, stop + 1)
    pmfs = np.zeros_like(x_grid, dtype=float)
    progress_func = tqdm if show_progress else lambda x: x
    for i in progress_func(range(len(x_grid))):
        pmfs[i] = marginal_wealth_distribution(x_grid[i])
    return x_grid, pmfs


def get_pareto_fit(wealths, pmfs):
    min_wealth = np.min(wealths)
    alpha = 1 / np.sum((pmfs * np.log(wealths / min_wealth)))
    return min_wealth, alpha


def plot_marginal_wealth(marginal_wealth_dist, start, stop, fit=False):
    fig = plt.figure()
    ax = fig.gca()
    counts, pmfs = evaluate_marginal_wealth(marginal_wealth_dist, start, stop)
    # wealths = simulation_params.c0 * np.exp2(counts)
    # plt.scatter(wealths, pmfs)
    plt.scatter(counts, pmfs)
    # ax.set_yscale('log')
    # ax.set_xscale('log')

    # if fit:
        # min_wealth, alpha = get_pareto_fit(wealths, pmfs)
        # print(min_wealth)
        # print(alpha)
        # wealth_grid = np.linspace(np.min(wealths), np.max(wealths), 1000)
        # pareto_dist = pareto(b=1, scale=1)
        # computed_pdfs = pareto_dist.pdf(wealth_grid)
        # plt.plot(wealth_grid[1:], computed_pdfs[1:])


def plot_multivariate_distribution(distribution, talent_axis, wealth_axis, title):

    talent_grid, wealth_grid = np.meshgrid(talent_axis, wealth_axis, indexing='ij')

    density_grid = np.zeros_like(talent_grid, dtype=float)
    for r in tqdm(range(len(talent_axis))):
        talent = talent_axis[r]
        talent_conditioned_dist = distribution(talent)
        for c in range(len(wealth_axis)):
            wealth = wealth_axis[c]
            density_grid[r, c] = talent_conditioned_dist(wealth)

    fig = plt.figure()
    ax = fig.gca()

    pandas_density = pd.DataFrame(density_grid, columns=wealth_axis, index=talent_axis)
    sns.heatmap(pandas_density, ax=ax)

    min_event_count, max_event_count = [wealth_axis[0], wealth_axis[-1]]
    min_talent, max_talent = [talent_axis[0], talent_axis[-1]]
    talent_label_spacing = 0.1

    wealth_label_count = 20
    ax.invert_yaxis()

    x_axis_size = ax.get_xlim()[1]
    y_axis_size = ax.get_ylim()[1]

    ax.set_xlabel(EVENT_LABEL)
    ax.set_ylabel('Talent')
    ax.set(title=title)
    ax.yaxis.set_major_formatter(ValueAxisFormatter(min_talent, max_talent, y_axis_size, '%.1f'))
    ax.yaxis.set_major_locator(MultipleLocator(int(talent_label_spacing * (max_talent - min_talent) * y_axis_size)))

    # fig = plt.figure()
    # ax = fig.gca(projection='3d')
    # # Plot the surface.
    # surf = ax.plot_surface(talent_grid, wealth_grid, density_grid, cmap=cm.coolwarm,
    #                        linewidth=0, antialiased=False)
    #
    # # Customize the z axis.
    # # ax.set_zlim(-1.01, 1.01)
    # ax.zaxis.set_major_locator(LinearLocator(10))
    # ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
    #
    # ax.set_xlabel('Talent')
    # ax.set_ylabel('Wealth')
    # ax.set_zlabel('Density')
    #
    # # Add a color bar which maps values to colors.
    # fig.colorbar(surf, shrink=0.5, aspect=5)


def show_p_hacking(simulation_params):
    talent_dist, conditional_wealth_dist_family, joint_distribution = get_distributions(simulation_params)
    talent_axis = np.linspace(0, 1, 1000)
    wealth_axis = np.arange(-30, 20 + 1)

    plot_multivariate_distribution(
        make_multivariate_from_talent(talent_dist), talent_axis, wealth_axis,
        'Talent Distribution'
    )

    plot_multivariate_distribution(
        conditional_wealth_dist_family, talent_axis, wealth_axis,
        'Wealth vs Talent Conditioned on Skill'
    )

    plot_multivariate_distribution(
        joint_distribution, talent_axis, wealth_axis,
        'Population Distribution of Wealth vs Talent'
    )
    show_plot()


def show_zero_luck_func():
    SAMPLE_COUNT = 1000000
    FIT_RESOLUTION = 1000
    BIN_COUNT = 100

    talent_mu = 0.6
    talent_sigma = 0.2
    wealth_min = 1
    wealth_alpha = 1.3

    # Sample the talents and plot with pdf
    plt.figure()
    talent_dist = norm(loc=talent_mu, scale=talent_sigma)
    talent_samples = talent_dist.rvs(size=SAMPLE_COUNT)
    talent_fit_grid = np.linspace(np.min(talent_samples), np.max(
        talent_samples), FIT_RESOLUTION)
    talent_pdf = talent_dist.pdf(talent_fit_grid)
    plt.hist(talent_samples, bins=BIN_COUNT, density=True)
    plt.plot(talent_fit_grid, talent_pdf)
    plt.xlabel('Talent')
    plt.ylabel('Probability Density')

    # Plot transform function
    plt.figure()
    wealth_dist = pareto(scale=wealth_min, b=wealth_alpha)
    def transform_talents(talents):
        return wealth_dist.ppf(talent_dist.cdf(talents))
    wealths_per_talent = transform_talents(talent_fit_grid)
    plt.plot(talent_fit_grid, wealths_per_talent)
    plt.xlabel('Talent')
    plt.ylabel('Resulting Wealth')

    # Plot transformed distribution
    plt.figure()
    transformed_wealths = transform_talents(talent_samples)
    transformed_wealths = transformed_wealths[transformed_wealths < 10 ** 3]
    log_bins = np.logspace(np.log10(np.min(transformed_wealths)), np.log10(np.max(
        transformed_wealths)), BIN_COUNT)
    plt.hist(transformed_wealths, bins=log_bins, density=True)
    wealth_fit_grid = np.logspace(
        np.log10(np.min(transformed_wealths)),
        np.log10(np.max(transformed_wealths)),
        FIT_RESOLUTION
    )
    wealth_fit_pdf = wealth_dist.pdf(wealth_fit_grid)
    plt.plot(wealth_fit_grid, wealth_fit_pdf)
    plt.xscale('log')
    plt.xlabel('Wealth')
    plt.ylabel('Probability Density')
    plt.show()


def show_wealth_dists_across_talents(simulation_params, talents=(0.3, 0.6, 0.9)):
    _, conditional_wealth, joint_distribution = get_distributions(simulation_params)
    marginal_wealth = get_marginal_wealth_distribution(joint_distribution)
    x_grid = np.linspace(-30, 30, 1000)
    for talent in talents:
        talent_marginal_wealth = conditional_wealth(talent)
        counts, pmfs = evaluate_marginal_wealth(talent_marginal_wealth, -30, 30)
        plt.plot(x_grid, spline_interpolate(counts, pmfs, x_grid), label=f'Talent: {talent}')
    counts, pmfs = evaluate_marginal_wealth(marginal_wealth, -30, 30)
    plt.plot(x_grid, spline_interpolate(counts, pmfs, x_grid), label='Overall')
    plt.xlabel(EVENT_LABEL)
    plt.ylabel('Density')
    plt.title('Wealth Distributions Across Talents')
    plt.legend()
    show_plot()

def show_average_wealth_across_talents(simulation_params, plot_wealths=True):
    _, conditional_wealth, joint_distribution = get_distributions(simulation_params)
    x_grid = np.linspace(0, 1, 1000)
    average_wealths = []
    for talent in tqdm(x_grid):
        talent_marginal_wealth = conditional_wealth(talent)
        counts, pmfs = evaluate_marginal_wealth(talent_marginal_wealth, -30, 30, show_progress=False)
        average_count = np.sum(counts * pmfs)
        average = simulation_params.c0 * np.exp2(average_count) if plot_wealths else average_count
        average_wealths.append(np.sum(average * pmfs))
    plt.plot(x_grid, average_wealths)
    plt.xlabel('Talent')
    plt.ylabel('Average Wealth')
    plt.title('Average Wealth Across Talents')
    show_plot()

def get_distributions(simulation_params):
    talent_dist = get_talent_dist(simulation_params.mu, simulation_params.sigma)
    conditional_wealth_dist_family = get_conditional_wealth_dist_family(
        simulation_params.pu,
        simulation_params.pl,
        simulation_params.iters,
        utilize_probability=simulation_params.utilize_probability
    )
    joint_distribution = get_joint_distribution(talent_dist, conditional_wealth_dist_family)
    return talent_dist, conditional_wealth_dist_family, joint_distribution


def junk():
    # Baseline wealth distribution for talent = 1
    plt.figure()
    conditional_wealth = get_distributions(SimulationParams().with_pl(1/3).with_pu(1/3))[1]
    talent_marginal_wealth = conditional_wealth(1)
    counts, pmfs = evaluate_marginal_wealth(talent_marginal_wealth, -30, 30)
    plt.scatter(counts, pmfs, label=f'Wealth')
    plt.xlabel(EVENT_LABEL)
    plt.ylabel('Density')
    plt.title('Wealth Distribution')
    # plt.yscale('log')
    plt.legend()

    plt.figure()
    # x_grid = np.arange(0, 41, 1)
    # ys = (40 - x_grid) / (41 + x_grid)
    # plt.scatter(x_grid[1:], np.abs(np.diff(ys)))

    # x_grid = np.arange(0, 81)
    # ys = factorial(80) / (factorial(x_grid) * factorial(80 - x_grid))
    # plt.scatter(x_grid, ys)

    # N = 80
    # x_grid = np.arange(0, 81)
    # N_factorial = factorial(N)
    # ys = (N_factorial * (N - x_grid + 1) - N_factorial * x_grid) / (factorial(x_grid) * factorial(N - x_grid + 1))
    # plt.scatter(x_grid, ys)

    # plt.figure()
    # N = 80
    # for L in np.arange(10, 30, 5):
    #     x_grid = np.linspace(0, (N - L) // 2, 1000)
    #     ys = (N - L - 2 * x_grid) / (L + x_grid + 1)
    #     plt.scatter(x_grid, ys, label=f'L={L}')
    #     plt.xlabel('Term (n)')
    #     plt.ylabel('Ratio')
    #     plt.legend()


def show_always_power_law(mu_range, sigma_range, prob_range, ratio_range, outer=('sigma', 'mu'), inner=('pu', 'pl')):

    def compute(simulation_params):
        marginal_wealth = get_marginal_wealth_distribution(get_distributions(simulation_params)[-1])
        counts, pmfs = evaluate_marginal_wealth(marginal_wealth, 1, 8, show_progress=False)
        wealths = simulation_params.c0 * np.exp2(counts)
        log_wealths = np.log(wealths)
        log_counts = np.log(counts)
        slope, intercept, *_ = linregress(log_wealths, log_counts)
        error = np.sum(np.abs(slope * log_wealths + intercept - log_counts)) / np.sum(log_counts)
        return error

    results = grid_compute(
        mu_range,
        sigma_range,
        prob_range,
        ratio_range,
        compute,
        '~/Desktop/checks/power_law_stop',
        load='~/Desktop/checks/power_law_load'
    )

    print(results)



def main():
    # show_p_hacking(SimulationParams())

    show_zero_luck_func()
    # show_always_power_law((0.1, 0.9), (0.1, 0.9), (0.1, 0.9), (0.1, 0.9))
    # show_wealth_dists_across_talents(SimulationParams())
    # show_average_wealth_across_talents(SimulationParams(), plot_wealths=False)

    # marginal_wealth = get_marginal_wealth_distribution(get_distributions(SimulationParams())[-1])
    # plot_marginal_wealth(marginal_wealth, 1, 8, fit=False)

    # N = 1000
    # params = SimulationParams().with_pu(0.04).with_pl(0.04)
    # marginal_wealth = get_marginal_wealth_distribution(get_distributions(params)[-1])
    # counts, pmfs = evaluate_marginal_wealth(marginal_wealth, 1, 8)
    # print(counts)
    # print(pmfs)
    # wealths = params.c0 * np.exp2(counts)
    # log_wealths = np.log(wealths)
    # counts = pmfs * N
    # log_counts = np.log(counts)
    # plt.scatter(wealths, counts)
    # slope, intercept, *_ = linregress(log_wealths, log_counts)
    # x_grid = np.linspace(log_wealths[0], log_wealths[-1], 1000)
    # y_fit = slope * x_grid + intercept
    # total_error = np.sum(np.abs(slope * log_wealths + intercept - log_counts))
    # print(total_error)
    # print(slope)
    # # plt.plot(x_grid, y_fit)
    #
    # plt.xscale('log')
    # plt.yscale('log')
    # plt.xlabel('log(Wealth)')
    # plt.ylabel('log(count)')





    show_plot()


if __name__ == '__main__':
    main()

