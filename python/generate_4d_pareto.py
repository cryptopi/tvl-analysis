from scipy.stats import linregress

from grid_compute import grid_compute, get_results_from_array, ComputeTree
from simulation_params import SimulationParams
from tvl import get_marginal_wealth_distribution, get_distributions, evaluate_marginal_wealth
import numpy as np
import matplotlib.pyplot as plt


def get_pareto_info(mu, sigma, total_probability, event_ratio):
    pu = event_ratio * total_probability
    pl = total_probability - pu

    simulation_params = SimulationParams(pu=pu, pl=pl, mu=mu, sigma=sigma)

    marginal_wealth = get_marginal_wealth_distribution(get_distributions(simulation_params)[-1])
    counts, pmfs = evaluate_marginal_wealth(marginal_wealth, 1, 8, show_progress=False)
    wealths = simulation_params.c0 * np.exp2(counts)
    log_wealths = np.log(wealths)
    log_pmfs = np.log(pmfs)
    slope, intercept, r_value, *_ = linregress(log_wealths, log_pmfs)
    error = np.sum(np.abs(slope * log_wealths + intercept - log_pmfs)) / np.abs(np.sum(log_pmfs))
    r_squared = r_value ** 2
    # print(slope)
    # print(error)
    #
    # plt.scatter(log_wealths, log_pmfs)
    # x_grid = np.arange(log_wealths[0], log_wealths[-1])
    # plt.plot(x_grid, slope * x_grid + intercept)
    # plt.show()

    return slope, r_squared, error


def get_pareto_slope(mu, sigma, total_probability, event_ratio):
    return get_pareto_info(mu, sigma, total_probability, event_ratio)[0]


def get_pareto_r2(mu, sigma, total_probability, event_ratio):
    return get_pareto_info(mu, sigma, total_probability, event_ratio)[1]


def get_pareto_error(mu, sigma, total_probability, event_ratio):
    return get_pareto_info(mu, sigma, total_probability, event_ratio)[2]


def plot_histogram(tree):
    tree = tree.prune()
    results = get_results_from_array(tree.to_array())
    print(results)
    plt.hist(results, bins=30)
    plt.show()


def gen_slopes():
    names = ('mu', 'sigma', 'total_probability', 'event_ratio')
    bounds = [(0.2, 0.8), (0.2, 0.8), (0.001, 0.2), (0.2, 0.8)]
    threads = 4

    watch_filename = '~/Desktop/explore/4d_pareto/std_func/slopes/watch'
    save_filename = '~/Desktop/explore/4d_pareto/std_func/slopes/save'
    load_filename = None

    grid_compute(names, bounds, get_pareto_slope, threads, watch_filename, save_filename, load_filename)


def gen_r2():
    names = ('mu', 'sigma', 'total_probability', 'event_ratio')
    bounds = [(0.2, 0.8), (0.2, 0.8), (0.001, 0.2), (0.2, 0.8)]
    threads = 4

    watch_filename = '~/Desktop/explore/4d_pareto/std_func/r2/watch'
    save_filename = '~/Desktop/explore/4d_pareto/std_func/r2/save'
    load_filename = None

    grid_compute(names, bounds, get_pareto_r2, threads, watch_filename, save_filename, load_filename)


def gen_errors():
    names = ('mu', 'sigma', 'total_probability', 'event_ratio')
    bounds = [(0.2, 0.8), (0.2, 0.8), (0.001, 0.2), (0.2, 0.8)]
    threads = 4

    watch_filename = '~/Desktop/explore/4d_pareto/std_func/errors/watch'
    save_filename = '~/Desktop/explore/4d_pareto/std_func/errors/save'
    load_filename = None

    grid_compute(names, bounds, get_pareto_error, threads, watch_filename, save_filename, load_filename)


if __name__ == '__main__':

    # gen_slopes()
    # gen_r2()
    # gen_errors()

    # plot_histogram(ComputeTree.read('~/Desktop/explore/4d_pareto/std_func/slopes/save'))
    plot_histogram(ComputeTree.read('~/Desktop/explore/4d_pareto/std_func/r2/save'))
    # plot_histogram(ComputeTree.read('~/Desktop/explore/4d_pareto/std_func/errors/save'))

    # print(get_pareto_info(0.6, 0.1, 0.4, 0.5))

