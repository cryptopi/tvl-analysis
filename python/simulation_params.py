import copy


class SimulationParams:

    def __init__(self, pu=0.4, pl=0.4, mu=0.6, sigma=0.1, c0=10, iters=80, utilize_probability=lambda t: 0.7 + t / 3):
        self.pu = pu
        self.pl = pl
        self.mu = mu
        self.sigma = sigma
        self.c0 = c0
        self.iters = iters
        self.utilize_probability = utilize_probability

    def copy(self):
        return copy.deepcopy(self)

    def with_member(self, name, value):
        copy = self.copy()
        copy.__setattr__(name, value)
        return copy

    def with_pu(self, pu):
        return self.with_member('pu', pu)

    def with_pl(self, pl):
        return self.with_member('pl', pl)

    def with_mu(self, mu):
        return self.with_member('mu', mu)

    def with_sigma(self, sigma):
        return self.with_member('sigma', sigma)

    def with_c0(self, c0):
        return self.with_member('c0', c0)

    def with_iters(self, iters):
        return self.with_member('iters', iters)

    def with_utilize_probability(self, utilize_probability):
        return self.with_member('utilize_probability', utilize_probability)
